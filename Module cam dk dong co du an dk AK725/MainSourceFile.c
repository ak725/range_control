./*
 * Project: Gia thu nghiem dk dong co BLDC
 * File:    newmainXC16.c
 * Vu Nguyen Minh H�ng
 * vnmhung@gmail.com
 * March 29, 2019, 3:35 PM
 */

// 29/12/2021
/* Code nay sua lai cho mach da duoc module hoa de cam vao plug
 * Sua lai mach nhu sau:
 * - Thay doi RS232 tu UART1(RX=1,TX=44) sang thanh UART2(RX=3, TX=2)
 * - Encoder tuyet doi chuyen sang d�ng RS485 th�ng qua UART1 Alternate (RX=35, TX=32)
 * Nhung cho sua lai trong code se co comment: //29/12/2021
 */

#define FCY 10000000UL
#define FPWM 15000

#pragma config FPR = XT_PLL8          // Primary Oscillator Mode (FRC w/ PLL 8x)
#pragma config FOS = PRI              // Oscillator Source (Internal Fast RC)
#pragma config FCKSMEN = CSW_FSCM_OFF // Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16  // WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512 // WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF      // Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_64     // POR Timer Value (64ms)
#pragma config BODENV = BORV20     // Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF    // PBOR Enable (Disabled)
#pragma config LPOL = PWMxL_ACT_HI // Low-side PWM Output Polarity (Active High)
#pragma config HPOL = PWMxH_ACT_HI // High-side PWM Output Polarity (Active High)
#pragma config PWMPIN = RST_PWMPIN // PWM Output Pin Reset (Control with HPOL/LPOL bits)
#pragma config MCLRE = MCLR_EN     // Master Clear Enable (Enabled)

// FGS
#pragma config GWRP = GWRP_OFF     // General Code Segment Write Protect (Disabled)
#pragma config GCP = CODE_PROT_OFF // General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD // Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include <xc.h>
#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define Start _RD1             //NUT AN START
//#define Stop  _RD0

#define Encoder_sh _RC13
#define Encoder_clk _RC14
#define Encoder_data _RB8

#define Lcd_rs LATDbits.LATD2 // CAU HINH CHO LCD
#define Lcd_rw LATFbits.LATF5
#define Lcd_en LATDbits.LATD3
#define Lcd_d4 LATFbits.LATF0
#define Lcd_d5 LATFbits.LATF1
#define Lcd_d6 LATFbits.LATF2 // pin 1 //29/12/2021
#define Lcd_d7 LATFbits.LATF3 // pin 44 //29/12/2021

#define LINE_1 0x80
#define LINE_2 0xC0
#define LINE_3 0x94
#define LINE_4 0xD4

#define Sample_time 100 // TG LAY MAU

/*----------------------------------------------------------------------------*/
/*--------------------------- Khai bao bien ----------------------------------*/
/*----------------------------------------------------------------------------*/

///////// CHU Y: CODE KENH HUONG or TAM ///////////
int kenh = 1; // code cho kenh Huong = 0; kenh Tam = 1

float Kp = 23, Ki = 25, Kd = 20;
double pPart = 0, iPart = 0, dpart = 0;
int Set_Ang = 0;
int pre_SetAng = 0;
int Set_Ang_spd = 0;
int Sensor_Data = 0;
int Actual_Ang = -1000;
double AngError = 0;
double preAngError = 0;
double preAngError1 = 0;
int DutyCycle = 0;
int DC_cal = 0;
double spd = 0;
int freq = 10;
double pre_Ang = -1000;
double spd_cal = 0.0;

int Flag_run = 0, count = 0, Flag_rev = 0, Flag_G = 0, count1 = 100;
unsigned int hall_value;
char buff[25], buff1[25], buff2[25];
char gui_pc[10];
char cmd;

char buffer[4];
int ibuff;

int goc_dieu_chinh = 0;   // 15/11
int duty_cycle_min1 = 90; // 55
int duty_cycle_min2 = 100;
int duty_cycle_max = 800;

/*----------------------------------------------------------------------------*/
/*----------------------------- Khai bao ham ---------------------------------*/
/*----------------------------------------------------------------------------*/
void Port_Init(void);
void CNI_Init(void);
void PWM_Init(void);
void Display_init(void);
void Init_TMR1(void);
void Init_TMR3(void);
void KhoiDong(void);
void DungDC(void);
void DaoChieu(void);
void Init_UART1_Module(void);
void Init_UART2_Module(void);
void Uart2_write(char chr);
void Uart2_write_text(char *str);
void Uart_write_text_encoder_rs485();
void Read_enc(void);
double sign_func(double x);

void Lcd1602_puts(char *s);
void Lcd1602_putchar(unsigned int cX);
void Lcd1602_init(void);
void Lcd1602_send_command(unsigned char cX);
void Lcd1602_send_4bit_data(unsigned char cX);
void Lcd1602_enable(void);
void Lcd1602_gotoxy(unsigned char x, unsigned char y);

/*----------------------------------------------------------------------------*/
/*--------------------------- Timer1 Interrupt -------------------------------*/
/*----------------------------------------------------------------------------*/
int is_large_error = -1;
void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void)
{
    IFS0bits.T1IF = 0;
    count1--;

    if (count1 <= 0)
    {
        count1 = 2;

        if (kenh == 0) // kenh huong
        {
            // Goc gioi han huong: 32 do Trai -> 32 do Phai
            if (Set_Ang < 320 || Set_Ang > 3280)
            {
                // khong quay
                PDC1 = 820;
                PDC2 = 820;
                PDC3 = 820;
                return;
            }

            if (Set_Ang > Actual_Ang)
            {
                Flag_G = 1; // Tang g�c
                AngError = Set_Ang - Actual_Ang;
            }
            else
            {
                Flag_G = 0; // Giam g�c
                AngError = Actual_Ang - Set_Ang;
            }
        }
        else if (kenh == 1) // kenh tam
        {
            // Goc gioi han tam: (-3) do -> (+80) do
            if (Set_Ang < 3570 && Set_Ang > 800)
            {
                // khong quay
                PDC1 = 820;
                PDC2 = 820;
                PDC3 = 820;
                return;
            }

            if (Set_Ang >= 3570) // goc dat < 0
            {
                if (Set_Ang > Actual_Ang && Actual_Ang > 1800)
                {
                    Flag_G = 1; // Tang g�c tam
                    AngError = Set_Ang - Actual_Ang;
                }
                else if (Actual_Ang > Set_Ang)
                {
                    Flag_G = 0; // Giam g�c tam
                    AngError = Actual_Ang - Set_Ang;
                }
                else // (Set_Ang > Actual_Ang && Actual_Ang <= 1800)
                {
                    Flag_G = 0; // Giam g�c tam
                    AngError = Actual_Ang + (3600 - Set_Ang);
                }
            }
            else // set angle >= 0
            {
                if (Actual_Ang > 1800) // goc tam hien tai dang < 0
                {
                    Flag_G = 1; // Tang g�c tam
                    AngError = Set_Ang + (3600 - Actual_Ang);
                }
                else if (Set_Ang > Actual_Ang)
                {
                    Flag_G = 1; // Tang g�c tam
                    AngError = Set_Ang - Actual_Ang;
                }
                else // Set_Ang <= Actual_Ang < 1800
                {
                    Flag_G = 0; // Giam g�c tam
                    AngError = Actual_Ang - Set_Ang;
                }
            }
        }

        AngError = AngError / 10;

        if (Flag_G == 0)
            AngError = -AngError;

        if (AngError > -0.4 && AngError < 0.4)
        {
            pPart = 0;
            iPart = 0; //+= Ki * (AngError)/1000;
            dpart = 0; // Kd * (AngError - preAngError1);
            DutyCycle = 0;
        }
        else
        {
            if (abs(AngError) > 90) // large error
            {
                if (spd <= 500)
                {
                    // if(AngError >=0)
                    //     DutyCycle = duty_cycle_max;
                    // else
                    //     DutyCycle = -duty_cycle_max;
                    Kp = 10.0; // 4.0
                }
                else
                {
                    Kp = 5000 / spd; // 4.0
                }
                pPart = Kp * AngError;
                DutyCycle = pPart;
            }
            else if (abs(AngError) > 50) // large error
            {
                // if (spd <= 80)
                // {
                //     // if(AngError >=0)
                //     //     DutyCycle = duty_cycle_max;
                //     // else
                //     //     DutyCycle = -duty_cycle_max;
                //     Kp = 10.0; // 4.0
                // }
                // else if(spd<1000)
                // {
                //     Kp = 5.0; // 4.0
                // }
                // else
                //     Kp = 0.0;
                if (spd > 500)
                {
                    Kp = 5000 / spd;
                }
                else
                {
                    Kp = 10.0;
                }
                pPart = Kp * AngError + 100 * sign_func(AngError) * abs(Set_Ang_spd);
                DutyCycle = pPart;
            }
            else if (abs(AngError) > 30) // large error
            {
                // if(spd>1000)
                // {
                //     Kp = 2.0;  // 0
                // }
                // else if (spd > 300)
                // {
                //     Kp = 3.0;  // 1.0
                // }
                // else if (spd > 150)
                // {
                //     Kp = 5.0;  // 3.0
                // }
                if (spd > 400)
                {
                    Kp = 4000 / spd;
                }
                else
                {
                    // if(AngError >=0)
                    //     DutyCycle = duty_cycle_max/1.5;  // /1.5
                    // else
                    //     DutyCycle = -duty_cycle_max/1.5;   // /1.5
                    Kp = 12.0;
                }
                pPart = Kp * AngError + 100 * sign_func(AngError) * abs(Set_Ang_spd);
                DutyCycle = pPart;
            }
            else if (abs(AngError) > 15) // large error
            {
                // if(spd > 1000)
                // {
                //     Kp = 1.0;  // 0.0
                // }
                // else if(spd>450)
                // {
                //     Kp = 2.0;   //1.0
                // }
                // else if (spd > 350)
                // {
                //     Kp = 4.0;   // 1.0
                // }
                // else if (spd >= 250)
                // {
                //     Kp = 8.0; // 220519  // 6.0
                // }
                // else if (spd > 100)
                // {
                //     Kp = 10;   // 7.0
                // }
                if (spd >= 250)
                {
                    Kp = 2500 / spd;
                }
                else if (spd >= 40)
                {
                    Kp = 15; // 10.0
                }
                else
                {
                    // if(AngError >=0)
                    //     DutyCycle = duty_cycle_max/2.0;   // /2.0
                    // else
                    //     DutyCycle = -duty_cycle_max/2.0;   // /2.0
                    Kp = 25.0;
                }
                pPart = Kp * AngError + 100 * sign_func(AngError) * abs(Set_Ang_spd);
                DutyCycle = pPart;
            }
            else if (AngError > 6)
            {
                // if (spd > 600)
                // {
                //     Kp = 0.5;  // 0.0
                // }
                // else if (spd > 350)
                // {
                //     Kp = 4.0;   // 1.0
                // }
                // else if (spd > 200)
                // {
                //     Kp = 8.0;  // 8.0
                // }
                // else if (spd > 100)
                // {
                //     Kp = 12.0;  // 10.0
                // }
                if (spd > 200)
                {
                    Kp = 2000 / spd;
                }
                else if (spd >= 50)
                {
                    Kp = 10.0;
                }
                else
                {
                    // if(AngError >=0)
                    //     DutyCycle = duty_cycle_max/3.0;    // /3.0
                    // else
                    //     DutyCycle = -duty_cycle_max/3.0;    // /3.0
                    Kp = 20.0;
                }
                pPart = Kp * AngError + 100 * sign_func(AngError) * abs(Set_Ang_spd);
                DutyCycle = pPart;
            }
            else if (AngError < -6)
            {
                // if (spd > 500)
                // {
                //     Kp = 2.0;    // 0.1
                // }
                // else if (spd > 300)
                // {
                //     Kp = 3.0;   // 0.3
                // }
                // else if (spd > 200)
                // {
                //     Kp = 4.0;  // 3.0
                // }
                // else if (spd > 100)
                // {
                //     Kp = 6.0;
                // }
                if (spd > 200)
                {
                    Kp = 2000 / spd;
                }
                else if (spd >= 50)
                {
                    Kp = 10.0;
                }
                else
                {
                    // if(AngError >=0)
                    //     DutyCycle = duty_cycle_max/3.0;    // /3.0
                    // else
                    //     DutyCycle = -duty_cycle_max/3.0;    // /3.0
                    Kp = 20.0;
                }
                pPart = Kp * AngError + 100 * sign_func(AngError) * abs(Set_Ang_spd);
                ;
                DutyCycle = pPart;
            }
            else
            {
                if (spd_cal >= 450)
                {
                    if (abs(Set_Ang_spd) > 0)
                        DutyCycle = 0;
                    else
                    {
                        if(AngError>0)
                            DutyCycle = -abs(spd_cal) * AngError / 3.0; 
                        else
                            DutyCycle = -abs(spd_cal) * AngError / 10.0;
                    }
                        
                }
                else if (spd_cal > 300)
                {
                    if (abs(Set_Ang_spd) > 0)
                        DutyCycle = 0;
                    else
                    {
                        if(AngError>0)
                            DutyCycle = -abs(spd_cal) * AngError / 4.0; 
                        else
                            DutyCycle = -abs(spd_cal) * AngError / 12.0;
                    }
                }
                else if (spd_cal > 100)
                {
                    if (abs(Set_Ang_spd) > 0)
                        DutyCycle = 0;
                    else
                    {
                        Kp = 300 / spd; // 0
                        pPart = Kp * AngError + 200 * sign_func(AngError) * abs(Set_Ang_spd);
                        DutyCycle = pPart;
                    }
                }
                else if (spd_cal > 50)
                {
                    Kp = 4.0; // 3
                    pPart = Kp * AngError + 200 * sign_func(AngError) * abs(Set_Ang_spd);
                    DutyCycle = pPart;
                }
                else if (spd_cal > 10)
                {
                    Kp = 50.0; // 6
                    pPart = Kp * AngError + 150 * sign_func(AngError) * abs(Set_Ang_spd);
                    DutyCycle = pPart;
                }
                else
                {
                    // if(AngError >=0)
                    //     DutyCycle = duty_cycle_min2;
                    // else
                    //     DutyCycle = -duty_cycle_min2;
                    if (fabs(AngError) > 4)
                        Kp = 50.0;
                    else if (fabs(AngError) > 2)
                        Kp = 60.0;
                    else if (fabs(AngError) > 1)
                        Kp = 80.0;
                    else
                        Kp = 150.0;
                    pPart = Kp * AngError;
                    
                    if(abs(Set_Ang_spd>0))
                        duty_cycle_min1 = 350;
                    else
                        duty_cycle_min1 = 80;

                    if (pPart >= 0 && pPart < duty_cycle_min1)
                        pPart = duty_cycle_min1;
                    if (pPart < 0 && pPart > -duty_cycle_min1 - 10)
                        pPart = -duty_cycle_min1 - 10;
                    
                    DutyCycle = pPart + 150 * sign_func(AngError) * abs(Set_Ang_spd);
                }
            }
        }

        /* Bang so gia tri PWM sang dien ap dau ra:
         * 28 - 0.3 v
         * 820 - 2 V
         * 1600 - 4 v
         */

        // gioi han toc do khi toi gan gioi han
        if ((kenh == 0 && ((Actual_Ang >= 3200 && Set_Ang >= Actual_Ang) ||
                           (Actual_Ang <= 400 && Set_Ang <= Actual_Ang))) ||                                                                                      // chay cham khi toi gan goc gioi han huong
            (kenh == 1 && DutyCycle > 800 && ((Actual_Ang >= 740 && Set_Ang >= Actual_Ang) || (Actual_Ang <= 50 && (Set_Ang <= Actual_Ang || Set_Ang >= 3570))))) // chay cham khi toi gan goc gioi han tam
        {
            if (DutyCycle > 400)
                DutyCycle = 400;
            if (DutyCycle < -400)
                DutyCycle = -400;
        }

        DC_cal = DutyCycle;

        DutyCycle = 820 + 1.0 * DutyCycle;

        if (DutyCycle > 1600) // 4 V
        {
            DutyCycle = 1600;
        }
        if (DutyCycle < 0) // 0 V
        {
            DutyCycle = 0;
        }

        PDC1 = DutyCycle;
        PDC2 = DutyCycle;
        PDC3 = DutyCycle;

        preAngError1 = preAngError;
        preAngError = AngError;
    }
}
/*----------------------------------------------------------------------------*/
/*---------------------------- CNI Interrupt ---------------------------------*/
/*----------------------------------------------------------------------------*/
void __attribute__((__interrupt__, auto_psv)) _CNInterrupt(void)
{
    IFS0bits.CNIF = 0;
    hall_value = ((PORTB & 0x0038) >> 3);
    if (Flag_G == 0)
    {
        // OVDCON = Thuan[hall_value];
    }
    if (Flag_G == 1)
    {
        // OVDCON = Nghich[hall_value];
    }
    OVDCON = 0xffff; // sua 14/11/2021
}
/*----------------------------------------------------------------------------*/
/*---------------------------- UART Interrupt ---------------------------------*/
/*----------------------------------------------------------------------------*/
int is_dang_dat_gia_tri = 0;
void __attribute__((__interrupt__, auto_psv)) _U2RXInterrupt(void)
{
    // 29/12/2021 sua toan bo ham nay:
    while (IFS1bits.U2RXIF == 1) // 29/12/2021
    {
        IFS1bits.U2RXIF = 0; // 29/12/2021
        char temp = U2RXREG; // 29/12/2021

        // if( temp == 'a' || temp == 'b' || temp == 'c')
        // {
        //     cmd = temp;
        //     buffer[0] = '0';
        //     buffer[1] = '0';
        //     buffer[2] = '0';
        //     buffer[3] = '0';
        //     ibuff = 0;
        //     is_dang_dat_gia_tri = 0;
        //     return;
        // }

        if (temp == 'p') // dat gia tri
        {
            cmd = temp;
            buffer[0] = '0';
            buffer[1] = '0';
            buffer[2] = '0';
            buffer[3] = '0';
            ibuff = 0;
            is_dang_dat_gia_tri = 1;
            return;
        }

        if (is_dang_dat_gia_tri == 1 && ibuff <= 3)
        {
            if (temp >= '0' && temp <= '9')
            {
                buffer[ibuff] = temp;
                ibuff = ibuff + 1;
            }
            else
            {
                buffer[0] = '0';
                buffer[1] = '0';
                buffer[2] = '0';
                buffer[3] = '0';
                ibuff = 0;
                is_dang_dat_gia_tri = 0;
                return;
            }
        }

        if (ibuff >= 4)
        {
            ibuff = 0;
            int Temp_Set_Ang = (buffer[0] - '0') * 1000 + (buffer[1] - '0') * 100 + (buffer[2] - '0') * 10 + (buffer[3] - '0');
            if (Temp_Set_Ang >= 3600) // goc gioi han
            {
                Temp_Set_Ang = 3599;
            }
            if (Temp_Set_Ang <= 0) // goc gioi han
            {
                Temp_Set_Ang = 0;
            }

            Set_Ang = Temp_Set_Ang;

            Flag_rev = 1;

            //__delay_us(10);

            is_dang_dat_gia_tri = 0;
        }
    }
}

uint16_t buffer_enc[20];
int _index = 0;
uint16_t n1 = 0;
uint16_t n2 = 0;
void __attribute__((__interrupt__, auto_psv)) _U1RXInterrupt(void)
{
    // them phan nhan du lieu tu encoder rs485 //29/12/2021
    while (IFS0bits.U1RXIF == 1) // 29/12/2021
    {
        IFS0bits.U1RXIF = 0;     // 29/12/2021
        uint16_t temp = U1RXREG; // 29/12/2021

        //        sprintf(buff,"nhan: %d/", (int) temp);
        //        Lcd1602_gotoxy(0,3);
        //        Lcd1602_puts(buff);

        buffer_enc[_index] = temp;
        _index += 1;
        //        if(_index > 9){
        //            _index = 0;
        //        }
        if (temp == 0x04 && _index >= 8)
        {
            _index = 0;
            int _index_0 = 0;
            while (buffer_enc[_index_0] != 0x01)
            {
                _index_0 += 1;
                if (_index_0 + 7 > _index)
                {
                    _index = 0;
                    return;
                }
            }

            if ((buffer_enc[_index_0 + 6] == buffer_enc[_index_0 + 4] ^ buffer_enc[_index_0 + 5] ^ buffer_enc[_index_0 + 1] ^ buffer_enc[_index_0 + 2] ^ buffer_enc[_index_0 + 3]) &&
                (buffer_enc[_index_0] == 0x01 && buffer_enc[_index_0 + 7] == 0x04))
            {
                n1 = buffer_enc[_index_0 + 4];
                n2 = buffer_enc[_index_0 + 5];
                double t = (((n1 * 256.0) + n2) * 3600.0) / 8192.0;
                if (kenh == 0) // kenh huong
                {
                    // dieu chinh de quy 0 cho encoder kenh huong
                    t = t + 1800.0;
                    if (t >= 3600)
                    {
                        t = t - 3600.0;
                    }
                }
                Actual_Ang = t;
                // if(pre_Ang == -1000 && Actual_Ang !=-1000)
                // {
                //     Set_Ang = Actual_Ang;
                //     pre_SetAng = Actual_Ang;
                // }
                    
                spd = fabs(t - pre_Ang) * 100.0;
                spd_cal = (t - pre_Ang) * sign_func(AngError) * 100.0;

                pre_Ang = t;
            }

            return;
        }
    }
}
/*----------------------------------------------------------------------------*/
/*----------------------------------  MAIN  ----------------------------------*/
/*----------------------------------------------------------------------------*/
int main()
{
    Port_Init();
    CNI_Init();
    PWM_Init();
    Display_init();
    Init_TMR1();
    Init_UART1_Module();
    Init_UART2_Module(); // 29/12/2021

    __delay_us(100);

    // cai dat huong ban dau
    while(Actual_Ang == -1000)
    {
        Read_enc();
        __delay_us(100);
        Set_Ang = Actual_Ang;
        pre_SetAng = Set_Ang;
    }
    
    while (1)
    {
        Read_enc();
        
        if(abs(Set_Ang - pre_SetAng) == 0)
        {
            if(cnt<5)
                cnt ++;
            else
                Set_Ang_spd = 0;
        }
        else
        {
            cnt = 0;
            Set_Ang_spd = abs(Set_Ang - pre_SetAng);
        }
        if (Set_Ang_spd > 80)
            Set_Ang_spd = 80;
        pre_SetAng = Set_Ang;
        // Get_speed();

        // if( cmd == 'a' || 1)//sua 14/11/2021
        // {
        //     Flag_rev = 1;
        //     Flag_run = 1;
        // }

        // while(Flag_run == 1)
        // {
        //     if(Flag_rev == 1)
        //     {
        //         Flag_rev = 0;
        //         KhoiDong();
        //     }

        //     Read_enc();
        //     //Get_speed();

        //     if(cmd == 'b')
        //     {
        //         DungDC();
        //         Flag_rev = 0;
        //         Flag_run = 0;
        //     }
        // }
    }
}

/*----------------------------------------------------------------------------*/
/*---------------------------- Tinh goc ------------------------------*/
/*----------------------------------------------------------------------------*/
double sign_func(double x)
{
    if (x > 0)
        return 1.0;
    else if (x < 0)
        return -1.0;
    else
        return 0;
}

void Read_enc(void)
{
    // 29/12/2021 comment cho nay:
    Uart_write_text_encoder_rs485(); // Gui lenh toi encoder
    for (int i = 0; i < 10; i++)
    {
        gui_pc[i] = 0;
    }

    // sau nay co encoder rs485 se tinh toan goc o day sau
    int goc_int = Actual_Ang;
    if (goc_int == 0)
        sprintf(gui_pc, "-0000");
    if (goc_int < 10)
        sprintf(gui_pc, "-000%d", goc_int);
    else if (goc_int < 100)
        sprintf(gui_pc, "-00%d", goc_int);
    else if (goc_int < 1000)
        sprintf(gui_pc, "-0%d", goc_int);
    else
        sprintf(gui_pc, "-%d", goc_int);

    Uart2_write_text(gui_pc);
    for (int i = 0; i < 10; i++)
    {
        gui_pc[i] = 0;
    }

    if (DC_cal <= -1000)
        sprintf(gui_pc, "N%d/", abs(DC_cal));
    else if (DC_cal <= -100)
        sprintf(gui_pc, "N0%d/", abs(DC_cal));
    else if (DC_cal <= -10)
        sprintf(gui_pc, "N00%d/", abs(DC_cal));
    else if (DC_cal < 0)
        sprintf(gui_pc, "N000%d/", abs(DC_cal));
    else if (DC_cal == 0)
        sprintf(gui_pc, "0000/");
    else if (DC_cal < 10)
        sprintf(gui_pc, "000%d/", DC_cal);
    else if (DC_cal < 100)
        sprintf(gui_pc, "00%d/", DC_cal);
    else if (DC_cal < 1000)
        sprintf(gui_pc, "0%d/", DC_cal);
    else
        sprintf(gui_pc, "%d/", DC_cal);
    Uart2_write_text(gui_pc);

    for (int i = 0; i < 10; i++)
    {
        gui_pc[i] = 0;
    }

    int spd_int = (int)spd_cal;
    if (spd_int <= -1000)
        sprintf(gui_pc, "N%d", abs(spd_int));
    else if (spd_int <= -100)
        sprintf(gui_pc, "N0%d", abs(spd_int));
    else if (spd_int <= -10)
        sprintf(gui_pc, "N00%d", abs(spd_int));
    else if (spd_int < 0)
        sprintf(gui_pc, "N000%d", abs(spd_int));
    else if (spd_int == 0)
        sprintf(gui_pc, "0000");
    else if (spd_int < 10)
        sprintf(gui_pc, "000%d", spd_int);
    else if (spd_int < 100)
        sprintf(gui_pc, "00%d", spd_int);
    else if (spd_int < 1000)
        sprintf(gui_pc, "0%d", spd_int);
    else
        sprintf(gui_pc, "%d", spd_int);
    //    sprintf(gui_pc, "%d", spd);
    Uart2_write_text(gui_pc);

    // Print LCD:

    //    sprintf(buff1,"G.Tri dat:        ");
    //    Lcd1602_gotoxy(0,0);
    //    Lcd1602_puts(buff1);
    //    sprintf(buff1," %d", Set_Ang);
    //    Lcd1602_gotoxy(11,0);
    //    Lcd1602_puts(buff1);
    //
    //    sprintf(buff2,"G.Tri thuc:        ");
    //    Lcd1602_gotoxy(0,1);
    //    Lcd1602_puts(buff2);
    //    sprintf(buff2," %d", Actual_Ang);
    //    Lcd1602_gotoxy(11,1);
    //    Lcd1602_puts(buff2);
    //
    //    sprintf(buff2,"%d; %d; %x", n1,n2,n2);
    //    Lcd1602_gotoxy(0,3);
    //    Lcd1602_puts(buff2);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void KhoiDong(void)
{
    hall_value = ((PORTB & 0x0038) >> 3);
    if (Flag_G == 0)
    {
        // OVDCON = Thuan[hall_value];
    }
    if (Flag_G == 1)
    {
        // OVDCON = Nghich[hall_value];
    }

    OVDCON = 0xffff;

    PWMCON1 = 0x0777;
    Flag_run = 1;
    T1CONbits.TON = 1;
    // T1CON = 0x8030;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void DungDC(void)
{
    PWMCON1 = 0x0777; // ban dau l� 0x0770 - sua 15/11
    // T1CON = 0x8030;
    T1CONbits.TON = 1;
    OVDCON = 0xffff;
    int tam = 820;
    PDC1 = tam;
    PDC2 = tam;
    PDC3 = tam;
    cmd = 'e';
    Flag_run = 0;
    preAngError = 0;
    AngError = 0;
    Display_init();
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void DaoChieu(void)
{
    OVDCON = 0x0000;
    // T1CON = 0x0030;
}
/*----------------------------------------------------------------------------*/
/*------------------------------- Khoi tao Port ------------------------------*/
/*----------------------------------------------------------------------------*/
void Port_Init(void)
{
    LATE = 0x0000;
    TRISE = 0x0000;
    TRISC = 0x0000;
    TRISF = 0x0000;
    TRISB = 0xFFFF;
    TRISD = 0x0000;

    // 29/12/2021
    //    Encoder_sh = 1;
    //    Encoder_clk = 1;
    //    Encoder_data = 0;
}
/*----------------------------------------------------------------------------*/
/*---------------------------- Set CNI interrupt -----------------------------*/
/*----------------------------------------------------------------------------*/
void CNI_Init(void)
{
    ADPCFG = 0xFFFF;

    CNEN1bits.CN5IE = 1;
    CNEN1bits.CN6IE = 1;
    CNEN1bits.CN7IE = 1;
    CNPU1bits.CN5PUE = 1;
    CNPU1bits.CN6PUE = 1;
    CNPU1bits.CN7PUE = 1;

    IFS0bits.CNIF = 0; // clear CNIF
    IEC0bits.CNIE = 1;
}
/*----------------------------------------------------------------------------*/
/*-------------------------------- Set MPWM ----------------------------------*/
/*----------------------------------------------------------------------------*/
void PWM_Init(void)
{
    PWMCON1 = 0x0770; // Enable all PWM pairs in independent mode : PWM_OUTPUT_H  = PWM_OUTPUT_L
    // PTCONbits.PTCKPS = 3; // prescale=1:64 (0=1:1, 1=1:4, 2=1:16, 3=1:64)
    //  PTPER = FCY / (FPWM * PRESCALE) - 1
    OVDCON = 0xffff; // 15/11
    PTPER = 1000;    // 20ms PWM period (15-bit period value)
    int tam = 820;   // gia tri ban dau 100 // 15/11 s?a l?i de ra 2V
    PDC1 = tam;      // 1.5ms pulse width on PWM channel 1
    PDC2 = tam;      // 1.5ms pulse width on PWM channel 2
    PDC3 = tam;      // 1.5ms pulse width on PWM channel 3
    SEVTCMP = PTPER;
    PWMCON2 = 0x0F00;   // Postscale = 1:16
    PTCONbits.PTEN = 1; // Enable PWM time baseF

    // 15/11:
    PWMCON1 = 0x0777;
    T1CONbits.TON = 1;
    // T1CON = 0x8030;
}
/*----------------------------------------------------------------------------*/
/*------------------------------ Set Timer1 ----------------------------------*/
/*----------------------------------------------------------------------------*/
void Init_TMR1(void)
{
    T1CONbits.TON = 0;
    TMR1 = 0;          // Clear timer register
    PR1 = 10000;       // Set Timer 1 period (max value is 65535)
    IFS0bits.T1IF = 0; // Clear Timer1 Interrupt Flag
    // T1CON = 0x0030;     // internal Tcy/256 clock,  Prescale 11 --> 256  <=> T1CONbits.TCKPS = 3
    T1CONbits.TCKPS = 0; // Prescale = 1:1
    // Sampling time = 1/FCY * Prescale * PR1 = 1/10^7 * 1 * 5000 =  1*10^(-3) * 10 (s) = 1 ms
    IEC0bits.T1IE = 1; // Enable Timer 1 interrupt
    T1CONbits.TON = 1;
}
/*----------------------------------------------------------------------------*/
/*------------------------------- Set Timer3 ---------------------------------*/
/*----------------------------------------------------------------------------*/
void Init_TMR3(void)
{
    T3CON = 0x0030; // internal Tcy/256 clock (TMR3 = 10000 = 1s))
    TMR3 = 0;
    PR3 = 0x8000;
}
/*----------------------------------------------------------------------------*/
/*---------------------------- Khoi tao hien thi -----------------------------*/
/*----------------------------------------------------------------------------*/
void Display_init(void)
{
    Lcd1602_init();
    __delay_ms(50);
    Lcd1602_gotoxy(0, 0);
    //    //Lcd1602_puts("DE TAI NCKH NAM 2019");
    //    Lcd1602_puts("THIET BI THU NGHIEM ");
    //    __delay_ms(50);
    //    Lcd1602_gotoxy(0,1);
    //    Lcd1602_puts("  DK DONG CO BLDC  ");
    //    __delay_ms(50);
    //    Lcd1602_gotoxy(0,2);
    //    Lcd1602_puts("  ---------------- ");
    //    __delay_ms(50);
    //    Lcd1602_gotoxy(0,3);
    //    Lcd1602_puts("     BM KTD MTA    ");
}
/*----------------------------------------------------------------------------*/
/*----------------------------- Set Module UART1 -----------------------------*/
/*----------------------------------------------------------------------------*/
void Init_UART1_Module(void)
{
    // 29/12/2021
    U1MODE = 0x8400; // Su dung UART1 Alternate => 8400; UART1 mac dinh la 8000
    /* Enable, 8data, no parity, 1 stop    */
    U1STA = 0x8400; // Enable TX1;			//bit10=UTXEN
    U1BRG = 31;     // = (2*Fcy / baudrate)/16 - 1   // baud = 38400 bps @ Fcy = 10 MHz
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1;
}
/*----------------------------------------------------------------------------*/
/*----------------------------- Set Module UART2 //29/12/2021 ---------------*/
/*----------------------------------------------------------------------------*/
void Init_UART2_Module(void) // them ham nay
{
    // 29/12/2021
    U2MODE = 0x8000; /* Enable, 8data, no parity, 1 stop    */
    U2STA = 0x8400;  // Enable TX1;			//bit10=UTXEN
    U2BRG = 129;     // baud = 9600 bps @ Fcy = 10 MHz
    IFS1bits.U2RXIF = 0;
    IEC1bits.U2RXIE = 1;
}
/*----------------------------------------------------------------------------*/
/*------------------------ G?i 1 byte ra cong UART2 ---------------------------*/
/*----------------------------------------------------------------------------*/
void Uart2_write(char chr)
{
    while (!U2STAbits.TRMT)
        ;               // 29/12/2021
    U2STAbits.TRMT = 0; // 29/12/2021
    U2TXREG = chr;      // 29/12/2021
}
/*----------------------------------------------------------------------------*/
/*------------------------ G?i 1 chuoi ra cong UART2 --------------------------*/
/*----------------------------------------------------------------------------*/
void Uart2_write_text(char *str)
{
    int j = 0;
    while (j <= 5)
    {
        Uart2_write(str[j]);
        j++;
    }
}
/*----------------------------------------------------------------------------*/
/*------------------------ G?i 1 byte ra encoder rs485 //29/12/2021---------------------------*/
/*----------------------------------------------------------------------------*/
void Uart_write_encoder_rs485(char chr) // 29/12/2021 them ham nay
{
    while (!U1STAbits.TRMT)
        ;               // 29/12/2021
    U1STAbits.TRMT = 0; // 29/12/2021
    U1TXREG = chr;      // 29/12/2021
}
/*----------------------------------------------------------------------------*/
/*------------------------ G?i 1 chuoi ra encoder rs485 //29/12/2021--------------------------*/
/*----------------------------------------------------------------------------*/
void Uart_write_text_encoder_rs485() // 29/12/2021 them ham nay
{
    Uart_write_encoder_rs485(0x01);
    Uart_write_encoder_rs485(0x80);
    Uart_write_encoder_rs485(0x02);
    Uart_write_encoder_rs485(0x80);
    Uart_write_encoder_rs485(0x04);
}
/*----------------------------------------------------------------------------*/
/*--------------------------- Giao tiep LCD 16x2 -----------------------------*/
/*----------------------------------------------------------------------------*/
void Lcd1602_enable(void)
{
    Lcd_en = 1;
    __delay_us(50);
    Lcd_en = 0;
    __delay_us(50);
}
/*----------------------------------------------------------------------------*/
void Lcd1602_send_4bit_data(unsigned char cX)
{
    Lcd_d4 = cX & 0x01;
    Lcd_d5 = (cX >> 1) & 1;
    Lcd_d6 = (cX >> 2) & 1;
    Lcd_d7 = (cX >> 3) & 1;
}
/*----------------------------------------------------------------------------*/
void Lcd1602_send_command(unsigned char cX)
{
    Lcd1602_send_4bit_data(cX >> 4); // send 4 bit high
    Lcd1602_enable();
    Lcd1602_send_4bit_data(cX); // send 4 bit low
    Lcd1602_enable();
}
/*----------------------------------------------------------------------------*/
void Lcd1602_init(void)
{
    Lcd1602_send_4bit_data(0x00);
    __delay_ms(50);

    Lcd_rs = 0;
    Lcd_rw = 0;
    Lcd_en = 0; // che do gui lenh

    __delay_us(30);

    Lcd1602_send_4bit_data(0x03); // ket noi 8 bit
    Lcd1602_enable();
    Lcd1602_enable();
    Lcd1602_enable();
    Lcd1602_send_4bit_data(0x02); // ket noi 4 bit
    Lcd1602_enable();

    Lcd1602_send_command(0x2C); // giao thuc 4 bit, hien thi 2 hang, ki tu 5x8
    Lcd1602_send_command(0x80);
    Lcd1602_send_command(0x0C); // cho phep hien thi man hinh
    Lcd1602_send_command(0x06); // tang ID, khong dich khung hinh
    Lcd1602_send_command(0x01); // xoa toan bo khung hinh
}
/*----------------------------------------------------------------------------*/
void Lcd1602_putchar(unsigned int cX)
{
    Lcd_rs = 1;
    Lcd1602_send_command(cX);
    Lcd_rs = 0;
}
/*----------------------------------------------------------------------------*/
void Lcd1602_puts(char *s)
{
    while (*s)
    {
        Lcd1602_putchar(*s);
        s++;
        __delay_us(30);
    }
}

/*----------------------------------------------------------------------------*/
void Lcd1602_gotoxy(unsigned char x, unsigned char y)
{
    unsigned char address;
    if (y == 0)
    {
        address = (LINE_1 + x);
    }
    else if (y == 1)
    {
        address = (LINE_2 + x);
    }
    else if (y == 2)
    {
        address = (LINE_3 + x);
    }
    else if (y == 3)
    {
        address = (LINE_4 + x);
    }
    __delay_us(30);
    Lcd1602_send_command(address);
    __delay_us(30);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
